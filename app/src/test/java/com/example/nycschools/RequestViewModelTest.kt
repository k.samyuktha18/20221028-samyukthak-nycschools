package com.manager.retrofitesting

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.Model.ModelScores
import com.example.nycschools.Repository.MainRepository
import com.example.nycschools.ViewModel.MainViewModel
import com.example.nycschools.WebService.RetrofitService
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.net.HttpURLConnection

@RunWith(MockitoJUnitRunner::class)
class RequestViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel

    private lateinit var apiHelper: MainRepository

    @Mock
    private lateinit var apiSchoolObserver: Observer<List<ModelSchool>>
    @Mock
    private lateinit var apiScoreObserver: Observer<List<ModelScores>>

    private lateinit var mockWebServer: MockWebServer
    @Mock
    private lateinit var retrofitService:RetrofitService


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        apiHelper = MainRepository(RetrofitService.getInstance)
        viewModel = MainViewModel(apiHelper)
       viewModel.getSchoolList().observeForever(apiSchoolObserver)
        viewModel.getScoreList().observeForever(apiScoreObserver)
        mockWebServer = MockWebServer()
        mockWebServer.start()

    }

    @Test
    fun `read sample success json file`(){
        val reader = MockResponseFileReader("success_response.json")
        assertNotNull(reader.content)
    }

    @Test
    fun `fetch details and check response Code 200 returned for school`(){
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val  actualResponse = apiHelper.getAllSchools().execute()
        // Assert
        assertEquals(response.toString().contains("200"),actualResponse.code().toString().contains("200"))
    }
    @Test
    fun `fetch details and check response Code 200 returned for scores`(){
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val  actualResponse = apiHelper.getAllScores()?.execute()
        // Assert
        assertEquals(response.toString().contains("200"),actualResponse.code().toString().contains("200"))
    }


    private fun `parse mocked JSON response`(mockResponse: String): String {
        val reader = JSONObject(mockResponse)
        return reader.getString("status")
    }

    @Test
    fun `fetch details for failed response 400 returned for school`(){
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
            .setBody(MockResponseFileReader("failed_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val  actualResponse = apiHelper.getAllSchools().execute()

        // Assert
        assertEquals(response.toString().contains("400"),actualResponse.toString().contains("400"))
    }
    @Test
    fun `fetch details for failed response 400 returned for scores`(){
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
            .setBody(MockResponseFileReader("failed_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val  actualResponse = apiHelper.getAllScores().execute()
        // Assert
        assertEquals(response.toString().contains("400"),actualResponse.toString().contains("400"))
    }
    @After
    fun tearDown() {
        viewModel.getSchoolList().removeObserver(apiSchoolObserver)
        viewModel.getScoreList().removeObserver(apiScoreObserver)
        mockWebServer.shutdown()
    }

}