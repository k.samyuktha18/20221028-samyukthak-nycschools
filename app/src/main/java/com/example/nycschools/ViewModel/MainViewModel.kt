package com.example.nycschools.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.Model.ModelScores
import com.example.nycschools.Repository.MainRepository
import com.example.nycschools.WebService.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// In the ViewModel I am creating a class and extending the ViewModel.
// ViewModel class here includes the business logic and API call implementations.
// In the ViewModel constructor, I am passing the data repository to handle the data.
class MainViewModel constructor(private val repository: MainRepository) : ViewModel(){

    val schoolList = MutableLiveData<List<ModelSchool>>()
    val scoresList = MutableLiveData<List<ModelScores>>()
    val errorMessage = MutableLiveData<String>()
//Using live data for posting the school response to UI

    fun getAllSchools(){
        val apiHelper = MainRepository(RetrofitService.getInstance)
        val response = apiHelper.getAllSchools()
        response.enqueue(object : Callback<List<ModelSchool>> {
            override fun onResponse(call: Call<List<ModelSchool>>, response: Response<List<ModelSchool>>) {
                schoolList.postValue(response.body())
            }
        //If Response if failed it goes to the failure
            override fun onFailure(call: Call<List<ModelSchool>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
    //Using live data for posting the scores response to UI

    fun getAllScores(){
        val apiHelper = MainRepository(RetrofitService.getInstance)
        val response = apiHelper.getAllScores()
        response.enqueue(object : Callback<List<ModelScores>> {
            override fun onResponse(call: Call<List<ModelScores>>, response: Response<List<ModelScores>>) {
                scoresList.postValue(response.body())
            }
            //If Response if failed it goes to the failure
            override fun onFailure(call: Call<List<ModelScores>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    @JvmName("getSchoolList1")
   fun getSchoolList(): MutableLiveData<List<ModelSchool>> {
        return schoolList
    }

    fun getScoreList(): MutableLiveData<List<ModelScores>> {
        return scoresList
    }
}