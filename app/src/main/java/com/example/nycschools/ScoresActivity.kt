package com.example.nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.Model.ModelScores
import com.example.nycschools.Repository.MainRepository
import com.example.nycschools.ViewModel.MainViewModel
import com.example.nycschools.ViewModel.ViewModelFactory
import com.example.nycschools.WebService.RetrofitService
import com.example.nycschools.WebService.RetrofitService.Companion.retrofitServiceScores

class ScoresActivity : AppCompatActivity() {
    private lateinit var viewModelScores: MainViewModel
    private lateinit var mathScore:TextView
    private lateinit var writingScore:TextView
    private lateinit var readingScore:TextView
    val dataList:ArrayList<ModelScores> = arrayListOf()
    var dbn:String? = null
    private val retrofitServiceScores= RetrofitService.getInstance
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scores)
        mathScore = findViewById(R.id.maths_score)
        writingScore = findViewById(R.id.writing_score)
        readingScore = findViewById(R.id.reading_score)
        dbn = intent.getStringExtra("DBN").toString()
        viewModelScores = ViewModelProvider(this,
            ViewModelFactory(MainRepository(retrofitServiceScores))
        ).get(MainViewModel::class.java)
        viewModelScores.scoresList.observe(this, Observer {
            it?.let { score->
                if(score.isNotEmpty())
                {
                    for(id in score)
                    {
                        val score = ModelScores(id.dbn,id.sat_math_avg_score,id.sat_critical_reading_avg_score,id.sat_writing_avg_score)
                        dataList.add(score)
                    }
                    matchedSchool(dataList) // method matches two lists
                }
            }

        })
        viewModelScores.errorMessage.observe(this, Observer {
            println("Error Occurred  $it")
        })
          viewModelScores.getAllScores()
    }

    fun matchedSchool(data:ArrayList<ModelScores>)
    {
        // filters matched dbn
        val scoreData:ModelScores? = data.find { it.dbn.equals(dbn,true) }

        // displaying scores in textview
       if(scoreData != null){
            mathScore.setText(scoreData.sat_math_avg_score)
           readingScore.setText(scoreData.sat_critical_reading_avg_score)
           writingScore.setText(scoreData.sat_writing_avg_score)
       }else
       {
          Toast.makeText(applicationContext,"No Scores Available",Toast.LENGTH_LONG).show()
       }
    }
}