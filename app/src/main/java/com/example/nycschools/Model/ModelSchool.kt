package com.example.nycschools.Model

// Holds the data called school name
data class ModelSchool(val dbn:String , val school_name:String)
