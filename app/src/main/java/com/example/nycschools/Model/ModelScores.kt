package com.example.nycschools.Model


// Holds the data of scores
data class ModelScores(val dbn:String,val sat_math_avg_score:String, val sat_critical_reading_avg_score:String, val sat_writing_avg_score:String)
