package com.example.nycschools.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.R
import com.example.nycschools.onRecyclerViewItemClick


// Adapter for showing the school list in RecyclerView
class CustomAdapter: RecyclerView.Adapter<CustomAdapter.MyViewHolder>() {
    var mList = ArrayList<ModelSchool>()
    private lateinit var listnerToSchoolName:onRecyclerViewItemClick
   inner class MyViewHolder(ItemView: View): RecyclerView.ViewHolder(ItemView){
       val schoolName = itemView.findViewById<TextView>(R.id.school_name)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view= LayoutInflater.from(parent.context).inflate(R.layout.school_list,parent, false)
        return MyViewHolder(view)

    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val data = mList.get(position)
        holder.schoolName.setText(data.school_name)
        holder.itemView.setOnClickListener(View.OnClickListener {
            listnerToSchoolName.onItemClick(data)
        })
    }
    override fun getItemCount(): Int {
        return  mList.size
    }

    fun replaceItems(lisnereToSchoolName:onRecyclerViewItemClick)
    {
        this.listnerToSchoolName = lisnereToSchoolName
    }
}