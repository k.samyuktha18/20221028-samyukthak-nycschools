package com.example.nycschools.Repository

import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.Model.ModelScores
import com.example.nycschools.WebService.RetrofitService
import retrofit2.Call

// I am using a repository pattern to handle the data from API.
// In the repository class, I am passing the retrofit service instance to perform the network call.
//  I am not handling the response here in the repository. That will be part of the ViewModel.
class MainRepository(private val apiInterface: RetrofitService) : RetrofitService {
    override fun getAllSchools(): Call<List<ModelSchool>> = apiInterface.getAllSchools()

    override fun getAllScores(): Call<List<ModelScores>> = apiInterface.getAllScores()


}