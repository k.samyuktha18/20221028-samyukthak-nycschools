package com.example.nycschools

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.Adapter.CustomAdapter
import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.Model.ModelScores
import com.example.nycschools.Repository.MainRepository
import com.example.nycschools.ViewModel.MainViewModel
import com.example.nycschools.ViewModel.ViewModelFactory
import com.example.nycschools.WebService.RetrofitService
interface onRecyclerViewItemClick{
    fun onItemClick(item:ModelSchool)
}
// Creating the instance of the viewmodel and observing the api response. Based on the api response I am updating the UI.
class MainActivity : AppCompatActivity(), onRecyclerViewItemClick{
    private lateinit var viewModelSchools: MainViewModel
    private lateinit var schoolRecyclerList:RecyclerView
    private val retrofitServiceSchools= RetrofitService.getInstance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        schoolRecyclerList = findViewById(R.id.school_recycler_data)
        viewModelSchools = ViewModelProvider(this,
            ViewModelFactory(MainRepository(retrofitServiceSchools))
        ).get(MainViewModel::class.java)
        viewModelSchools.schoolList.observe(this, Observer {
          print("GOT RESULT $it")
            loadAdapter(it)
        })


        viewModelSchools.errorMessage.observe(this, Observer {
            println("Error Occurred: $it")
        })

        viewModelSchools.getAllSchools()

    }
// Loads the data to adapter
    fun loadAdapter(list: List<ModelSchool>)
    {
        schoolRecyclerList.layoutManager = LinearLayoutManager(applicationContext)
        val adapter = CustomAdapter()
        adapter.replaceItems(this)
        adapter.mList = list as ArrayList<ModelSchool>
        schoolRecyclerList.adapter = adapter
    }


    // click of recycler view takes us to next activity for displaying scores.
    override fun onItemClick(item: ModelSchool) {
        val intent = Intent(this, ScoresActivity::class.java)
    // To pass any data to next activity
        intent.putExtra("DBN", item.dbn)
        // start your next activity
        startActivity(intent)
    }
}