package com.example.nycschools.WebService

import com.example.nycschools.Model.ModelSchool
import com.example.nycschools.Model.ModelScores
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
// First, creating the interface for the API call definition.
interface RetrofitService {

    @GET("s3k6-pzi2.json")
    fun getAllSchools(): Call<List<ModelSchool>>

    @GET("f9bf-2cp4.json")
    fun getAllScores(): Call<List<ModelScores>>

    companion object {
        var retrofitServiceSchools: RetrofitService? = null
        var retrofitServiceScores: RetrofitService? = null

        //Creating the Retrofit service instance using the retrofit.
        fun getRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    val getInstance:RetrofitService = getRetrofit().create(RetrofitService::class.java)
    }
}